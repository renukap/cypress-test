// ***********************************************
// Test verifies in each filter that 
// 1. Todo texts are correct 
// 2. Correct number of todos added
// 3. Correct number of active and complete todos are displayed
// ***********************************************
describe ('Verify list items', ()=>{
    beforeEach(()=>{
        cy.visit('/')
        cy.addNewItems()
    })

    it('Display all items in all tab', ()=>{
        const getTodos = (win) =>
            win.store.getState().todos
        cy.window().then(getTodos).should('have.length', 4)
        cy.get('li').first().should('have.length', 1).and('contain', 'Task 1')
        .and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
        cy.get('li:nth-child(2)').should('have.length', 1).and('contain', 'Task 2')
        .and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
        cy.get('li:nth-child(3)').should('have.length', 1).and('contain', 'Task 3')
        .and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
        cy.get('li:nth-child(4)').should('have.length', 1).and('contain', 'Task 4')
        .and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
    })

    it('Display incomplete items in active tab', ()=>{
        cy.get('button:nth-child(3)').click().should('be.disabled')
        cy.get('li').should('have.length', 4)
        cy.get('li').first().should('have.length', 1).and('contain', 'Task 1')
        .and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
        cy.get('li:nth-child(2)').should('have.length', 1).and('contain', 'Task 2')
        .and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
        cy.get('li:nth-child(3)').should('have.length', 1).and('contain', 'Task 3')
        .and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
        cy.get('li:nth-child(4)').should('have.length', 1).and('contain', 'Task 4')
        .and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
    })

    it('Display completed items in completed tab', ()=>{
        cy.get('button:nth-child(4)').click().should('be.disabled')
        cy.get('li').should('not.exist')
    })

})
// ***********************************************
// Test verifies  
// 1. Re-arrange todos by drag and drop
// ***********************************************
describe ('Re-arrange todos by drag and drop', ()=>{
    // runs before each test cases
    beforeEach(()=>{
        cy.visit('/')
        // Adding new item should be possible
        cy.addNewItems()
    })
    
    // Drag and drop first todo item to the second position 
    it('Selection of a todo  item', ()=>{
        cy.visit('/')
        const taskItem = 'New task 1'
        cy.get('input').type(taskItem).type('{enter}').should('have.value', '')
        cy.get('li').should('have.length', 1).and('contain', taskItem)
        cy.get('input').type('Test with Cypress{enter}')
        cy.contains('li', 'Test with Cypress').find('input[type=checkbox]').click()
    })

    it('Drag and drop an item', () => {
        const dataTransfer = new DataTransfer;
        cy.get('li').first()
            .trigger('dragstart', { dataTransfer });
        cy.get('li:nth-child(2)')
            .trigger('drop', { dataTransfer });
        cy.get('li:nth-child(1)')
            .trigger('dragend');
        cy.get('li:nth-child(2)').should('contain', 'Task 1');
    });
})
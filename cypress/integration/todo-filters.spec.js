// ***********************************************
// Test verifies following
// 1. verify filter links 
// 2. Add a new todo while specific filter is selected
// 3. Mark incomplete a todo while specific filter is selected
// 4. Complete a todo while specific filter is selected
// ***********************************************
describe ('Verify filters', ()=>{
    beforeEach(()=>{
        cy.visit('/')
        cy.server()
        cy.addNewItems()
        cy.toggleFirstItem()
    })

    it('verify filter links', ()=>{
        const filters = [
            {link: 'Active', expectedLength: 3},
            {link: 'Completed', expectedLength: 1},
            {link: 'All', expectedLength: 4}
        ]
        cy.wrap(filters)
            .each(filter => {
                cy.contains(filter.link).click()
                cy.get('li').should('have.length', filter.expectedLength)
            })
    })
    context('Add a new todo while specific filter is selected', ()=>{
        it('Verify while "All" is selected', ()=>{
            cy.addFourthItem()
            const filters = [
                {link: 'Active', expectedLength: 4},
                {link: 'Completed', expectedLength: 1},
                {link: 'All', expectedLength: 5}
            ]
            cy.wrap(filters)
                .each(filter => {
                    cy.contains(filter.link).click()
                    cy.get('li').should('have.length', filter.expectedLength)
                })
        })
        it('verify while "Active" is selected', ()=>{
            cy.addFourthItem()
            const filters = [
                {link: 'All', expectedLength: 5},
                {link: 'Completed', expectedLength: 1},
                {link: 'Active', expectedLength: 4}
            ]
            cy.contains('Active').click()
            cy.wrap(filters)
                .each(filter => {
                    cy.contains(filter.link).click()
                    cy.get('li').should('have.length', filter.expectedLength)
                })
        })
        it('verify while "Completed" is selected', ()=>{
            cy.addFourthItem()
            const filters = [
                {link: 'All', expectedLength: 5},
                {link: 'Completed', expectedLength: 1},
                {link: 'Active', expectedLength: 4}
            ]
            cy.contains('Active').click()
            cy.wrap(filters)
                .each(filter => {
                    cy.contains(filter.link).click()
                    cy.get('li').should('have.length', filter.expectedLength)
                })
        })
    })

    context('Mark incomplete a todo while specific filter is selected', ()=>{
        it('Verify while "All" is selected', ()=>{
            const filters = [
                {link: 'Active', expectedLength: 4},
                {link: 'Completed', expectedLength: 0},
                {link: 'All', expectedLength: 4}
            ]
            cy.get('li').first().should('have.length', 1).
            and('css', 'text-decoration', 'line-through solid rgb(0, 0, 0)')
            // Toggle first item
            cy.toggleFirstItem()

            cy.wrap(filters)
                .each(filter => {
                    cy.contains(filter.link).click()
                    cy.get('li').should('have.length', filter.expectedLength)
                })
        })

        it('Verify while "Completed" is selected', ()=>{
            const filters = [
                {link: 'All', expectedLength: 4},
                {link: 'Completed', expectedLength: 0},
                {link: 'Active', expectedLength: 4}
            ]
            cy.contains('Completed').click()
            cy.get('li').first().should('have.length', 1).
            and('css', 'text-decoration', 'line-through solid rgb(0, 0, 0)')
            cy.toggleFirstItem()
            cy.wrap(filters)
                .each(filter => {
                    cy.contains(filter.link).click()
                    cy.get('li').should('have.length', filter.expectedLength)
                })
        })
        it('Verify while "Active" is selected', ()=>{
            cy.contains('Active').click()
            cy.get('li').first().should('have.length', 1).
            and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
        })
    })    

    context('Complete a todo while specific filter is selected', ()=>{
        it('Verify while "All" is selected', ()=>{
            const filters = [
                {link: 'Active', expectedLength: 2},
                {link: 'Completed', expectedLength: 2},
                {link: 'All', expectedLength: 4}
            ]
            cy.get('li:nth-child(2)').should('have.length', 1).
            and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
            cy.toggleSecondItem()
            cy.wrap(filters)
                .each(filter => {
                    cy.contains(filter.link).click()
                    cy.get('li').should('have.length', filter.expectedLength)
                })
        })

        it('Verify while "Active" is selected', ()=>{
            const filters = [
                {link: 'Completed', expectedLength: 2},
                {link: 'All', expectedLength: 4},
                {link: 'Active', expectedLength: 2}
            ]
            cy.contains('Active').click()
            cy.get('li').first().should('have.length', 1).
            and('css', 'text-decoration', 'none solid rgb(0, 0, 0)')
            cy.toggleFirstItem()
            cy.wrap(filters)
                .each(filter => {
                    cy.contains(filter.link).click()
                    cy.get('li').should('have.length', filter.expectedLength)
                })
        })
        it('Verify while "Completed" is selected', ()=>{
            cy.contains('Completed').click()
            cy.get('li').first().should('have.length', 1).
            and('css', 'text-decoration', 'line-through solid rgb(0, 0, 0)')
        })
    })
})
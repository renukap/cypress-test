// ***********************************************
// Test verifies  
// 1. initial page load and verify the state
// 2. Focus on input field
// 3. No todos at initial page load 
// 4. Adding new item
// 5. Submitting the form without input
// 6. Input with special characters
// ***********************************************
describe ('Input form', ()=>{
    // runs before each test cases
    beforeEach(()=>{
        cy.visit('/')
    })
    // initial page load - Page should load successfully
    it('Initial Page load', ()=>{
        cy.visit('/')
        cy.get('form > button').should('have.attr', 'type', 'submit')
    })
    // Focus on input field - Input field should be focused
    it('Focus on input while page load', ()=>{
        cy.focused().should('should.have', 'form > input')
    })
    // No todos shown in initial load
    it('No todos are shown in initial load', ()=>{
        cy.get('li').should('not.exist')
        //cy.get(li)
        cy.window().its('store').invoke('getState').should('deep.equal', {
            todos: [],
            visibilityFilter: "SHOW_ALL"
        })
    })
    // Adding new item should be possible and verify the state
    context('Add new todos and verify the state', ()=>{
        it('No todos visible on the page', ()=>{
            //const taskItem = 'New task 1'
            //cy.get('input').type(taskItem).type('{enter}').should('have.value', '')
            cy.fixture('todo').then((todo)  => {
                var data1 = todo[0].text
                cy.get('input').type(data1).type('{enter}').should('have.value', '')
                cy.get('li').should('have.length', 1).and('contain', data1)
                cy.window().its('store').invoke('getState').should('deep.equal', {
                    todos: [
                        {
                            id: 0, 
                            text: "New task 1", 
                            completed: false
                        }
                    ],
                    visibilityFilter: "SHOW_ALL"
                })    
            })
        })
        // No todos should be added if no inputs are provided.
        it('Form submission fails with no input',()=>{
            cy.get('input').type('{enter}')
            cy.get('li').should('not.exist')
            cy.window().its('store').invoke('getState').should('deep.equal', {
                todos: [],
                visibilityFilter: "SHOW_ALL"
            })
    
        })
        it('Form submission should not allow special characters',()=>{
            cy.get('input').type("~`!@#$%^&*()-_=+[]}{|\\\"':;/?.>,<").type('{enter}')
            cy.get('li').should('not.exist')
            cy.window().its('store').invoke('getState').should('deep.equal', {
                todos: [],
                visibilityFilter: "SHOW_ALL"
            })
        })
        
    })
})
Cypress.Commands.add("visitApp", (nItems) => {
    cy.visit('/')
})

Cypress.Commands.add('addNewItems', ()=>{
    cy.fixture('todo').then((todo)  => {
        cy.get('input').type('Task 1{enter}').type('Task 2{enter}').type('Task 3{enter}').type('Task 4{enter}')
    })
})

Cypress.Commands.add('toggleFirstItem', ()=>{
    cy.get('li').first().click()
}) 

Cypress.Commands.add('toggleSecondItem', ()=>{
    cy.get('li:nth-child(2)').click()
})  
Cypress.Commands.add('addFourthItem', ()=>{
    cy.get('input').type('Task 5').type('{enter}')
})

